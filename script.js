// 1. Метод forEach виконує функцію callback один раз для кожного елемента який знаходиться в массиві та функція приймає
// від одного до трьох аргументів.

// 2. Очистити масив можно якщо довжині задати значення нуль. arr.lenght = 0;

// 3. Перевірити чи змінна є масивом можно завдяки isArray. arr.isArray([]) // true

let arr = ["hello", "world", 23, "23", 2, 3, "Vlad", { age: 23 }, null];
let type = "string";
function filterBy(arr, type) {
  return arr.filter((element) => {
    if (typeof element !== type) {
      return true;
    }
  });
}
console.log(filterBy(arr, "string"));
console.log(filterBy(arr, "number"));
console.log(filterBy(arr, "object"));

// let arr = ["hello", "world", 23, "23", 2, 3, "Vlad", { age: 23 }, null];
// let allTypes = ["string", "number", "object"];
// function filterBy(arr, allTypes) {
//   return arr.filter((element) => {
//     if (typeof element !== allTypes) {
//       return true;
//     }
//   });
// }
// allTypes.forEach((type) => console.log(filterBy(arr, type)));
